const path = require('path');

module.exports = {
  mode: 'production',
  entry: [path.join(__dirname, 'index.ts')],
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'geoflector.min.js',
    library: 'GeoFlector',
    libraryTarget: 'umd',
    libraryExport: 'default',
    globalObject: 'this',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /(node_modules)/,
        use: ['babel-loader', 'ts-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
};
