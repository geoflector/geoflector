module.exports = {
  base: '/geoflector/',
  head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
  theme: 'geoflector',
  dest: 'public',
  themeConfig: {
    logo: 'logo.svg',
    search: false,
    editLinks: false,
    activeHeaderLinks: true,
    nav: [
      { text: 'Demo', link: '/' },
      { text: 'Dokumentáció', link: '/docs' },
    ],
    repo: 'https://gitlab.com/geoflector/geoflector',
    repoLabel: 'Gitlab',
  },
  plugins: {
    'vuepress-plugin-typescript': {},
    'clean-urls': {
      normalSuffix: '',
      indexSuffix: '',
    },
    'google-analytics': {
      'ga': 'UA-6670432-15'
    }
  },
  extraWatchFiles: ['../../src'],
};
