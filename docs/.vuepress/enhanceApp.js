import InstantSearch from 'vue-instantsearch';
import GeoFlector  from '../../index.ts';

export default ({ Vue }) => {
  Vue.use(InstantSearch);
  Vue.directive('focus', {
    inserted(el) {
      el.querySelector('input').focus();
    },
  });
  Vue.mixin({
    methods: {
      getInflections(geoGraphicalName) {
        const geoFlector = new GeoFlector(geoGraphicalName);

        return [
          { primary: geoFlector.getInflection('Locativus'), secondary: 'locativus' },
          { primary: geoFlector.getInflection('Adjectivus'), secondary: 'adjectivus' },
          { primary: geoFlector.getInflection('Accusativus'), secondary: 'accusativus' },
          { primary: geoFlector.getInflection('Dativus'), secondary: 'dativus' },
          {
            primary: geoFlector.getInflection('InstrumentalisComitativus'),
            secondary: 'instrumentalis comitativus',
          },
          { primary: geoFlector.getInflection('CasualisFinalis'), secondary: 'casualis finalis' },
          { primary: geoFlector.getInflection('Translativus'), secondary: 'translativus' },
          { primary: geoFlector.getInflection('IllativusOrSublativus'), secondary: 'illativus or sublativus' },
          { primary: geoFlector.getInflection('ElativusOrDelativus'), secondary: 'elativus or delativus' },
          { primary: geoFlector.getInflection('Allativus'), secondary: 'allativus' },
          { primary: geoFlector.getInflection('Adessivus'), secondary: 'adessivus' },
          { primary: geoFlector.getInflection('Terminativus'), secondary: 'terminativus' },
        ];
      },
    },
  });
};
