# Dokumentáció

## Telepítés

#### CDN segítségével

```html
<script src="unpkg.com/geoflector@latest/lib/geoflector.min.js"></script>
```

#### npm csomagként

```shell script
# yarn
yarn add geoflector

# npm
npm i geoflector
```

## Példa

```javascript
import GeoFlector from 'geoflector';

const geoflector = new GeoFlector('Pécs');
const locativus = geoflector.getInflection('Locativus'); // Pécsett
```

## API

### ```new GeoFlector(geographicalName: string): GeoFlector```

Példányosítjuk a `GeoFlector` osztályt, hogy a tulajdonnév bármilyen toldalékát kinyerhessük

### ```geoFlector.getInflection(inflection: Inflection): string```

Visszatér a toldalékolt helységnévvel. Paraméterként a magyar nyelvtan egy esetének latin nevét várja, camel case
írásmóddal, tehát egy az alábbiak közül:
- `Ablativus`
- `Accusativus`
- `Adessivus`
- `Adjectivus`
- `Allativus`
- `CasualisFinalis`
- `Dativus`
- `Delativus`
- `Elativus`
- `ElativusOrDelativus`
- `EssivusFormalis`
- `EssivusModalis`
- `Illativus`
- `IllativusOrSublativus`
- `Inessivus`
- `InessivusOrSuperessivus`
- `InstrumentalisComitativus`
- `Locativus`
- `Nominativus`
- `Sublativus`
- `Superessivus`
- `Terminativus`
- `Translativus`

További információt a nyelvtani esetekről itt találsz:

[A magyar nyelv eseteinek listája (Wikipédia)](https://hu.wikipedia.org/wiki/A_magyar_nyelv_eseteinek_list%C3%A1ja)

