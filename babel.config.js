module.exports = {
  env: {
    test: {
      presets: [
        [
          '@babel/env',
          {
            modules: 'auto',
            targets: {
              esmodules: true,
              node: true,
              browsers: true,
            },
          },
        ],
      ],
    },
  },
};
