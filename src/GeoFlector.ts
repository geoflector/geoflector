import Word from './Word';
import { getSuffix } from './util/getSuffix';
import { getAdjective } from './util/getAdjective';
import { getVowelHeight } from './util/getVowelHeight';
import { getAssimilatedWord } from './util/getAssimilatedWord';
import { isBindingPhonemeNeeded } from './util/isBindingPhonemeNeeded';
import { getActualInflection } from './util/getActualInflection';
import { Inflection } from '../types';

export default class GeoFlector {
  geographicalName: string;

  constructor(geographicalName: string) {
    this.geographicalName = geographicalName;
  }

  private get phrases(): Array<string> {
    return this.geographicalName.split(Word.PHRASES_SEPARATOR);
  }

  private get wordToInflect(): Word {
    return new Word(this.phrases[this.phrases.length - 1]);
  }

  private get shouldCapitalize(): boolean {
    return this.phrases.length === 1 || /^[A-Z]*$/.test(this.wordToInflect.value.charAt(0));
  }

  private getInflectedWord(inflection: Inflection): string {
    const { wordToInflect: word } = this;

    if (inflection === 'Nominativus') return word.value;
    if (inflection === 'Adjectivus') return getAdjective(word);

    const actualInflection = getActualInflection(word, inflection);
    const assimilatedWord = getAssimilatedWord(word, actualInflection);
    const vowelHeight = getVowelHeight(word);
    const needConjunction = word.lastLetter.isVowel;
    const needBindingPhoneme = isBindingPhonemeNeeded(word, actualInflection);
    const stem = this.shouldCapitalize ? assimilatedWord.capitalized : assimilatedWord.value;

    return stem + getSuffix(actualInflection, vowelHeight, needConjunction, needBindingPhoneme);
  }

  getInflection(inflection: Inflection): string {
    const inflectedWord = this.getInflectedWord(inflection);
    return this.geographicalName.replace(this.wordToInflect.value, inflectedWord);
  }
}
