import GeoFlector from '../GeoFlector';
import { Inflection } from '../../types';

describe.each([
  ['Accusativus', 'Baks', 'Baksot'],
  ['Nominativus', 'Marosvásárhely', 'Marosvásárhely'],
  ['Accusativus', 'Marosvásárhely', 'Marosvásárhelyet'],
  ['Accusativus', 'Sopron', 'Sopront'],
  ['Accusativus', 'Szatymaz', 'Szatymazt'],
  ['Accusativus', 'Pécs', 'Pécset'],
  ['Accusativus', 'Ágasegyháza', 'Ágasegyházát'],
  ['Accusativus', 'Ásotthalom', 'Ásotthalmot'],
  ['Accusativus', 'Salföld', 'Salföldet'],
  ['Accusativus', 'Aldebrő', 'Aldebrőt'],
  ['Accusativus', 'Babarcszőlős', 'Babarcszőlőst'],
  ['Dativus', 'Alsónemesapáti', 'Alsónemesapátinak'],
  ['Adjectivus', 'Alsónemesapáti', 'alsónemesapáti'],
  ['Adjectivus', 'Eger', 'egri'],
  ['Adjectivus', 'Győr', 'győri'],
  ['ElativusOrDelativus', 'Csurgó', 'Csurgóról'],
  ['IllativusOrSublativus', 'Csurgó', 'Csurgóra'],
  ['InessivusOrSuperessivus', 'Ág', 'Ágon'],
  ['InessivusOrSuperessivus', 'Ágasegyháza', 'Ágasegyházán'],
  ['InessivusOrSuperessivus', 'Újfehértó', 'Újfehértón'],
  ['InessivusOrSuperessivus', 'Öskü', 'Öskün'],
  ['InessivusOrSuperessivus', 'Ózdfalu', 'Ózdfalun'],
  ['InessivusOrSuperessivus', 'Tófű', 'Tófűn'],
  ['InessivusOrSuperessivus', 'Nagyrábé', 'Nagyrábén'],
  ['InessivusOrSuperessivus', 'Kaskantyú', 'Kaskantyún'],
  ['InessivusOrSuperessivus', 'Kaposvár', 'Kaposváron'],
  ['Inessivus', 'Csokonai utca', 'Csokonai utcában'],
  ['Locativus', 'Kaposvár', 'Kaposvárott'],
  ['InstrumentalisComitativus', 'Ágasegyháza', 'Ágasegyházával'],
])('%p case', (inflection, original, expected) => {
  it(`should return ${expected}`, () => {
    const inflectionType = inflection as Inflection;
    expect(new GeoFlector(original).getInflection(inflectionType)).toBe(expected);
  });
});
