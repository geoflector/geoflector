import Word from '../Word';
import Letter from '../Letter';

describe('Word class', () => {
  it('get word vowels', () => {
    const word = new Word('hello');
    expect(word.vowels).toEqual([new Letter('e'), new Letter('o')]);
  });

  describe('word.vowelHarmony', () => {
    it('front', () => {
      const word = new Word('tok');
      expect(word.vowelHarmony).toBe('Back');
    });
  });

  describe('word.letters', () => {
    it('should takes into account the two digit letters', () => {
      const word = new Word('Pécs');
      expect(word.letters).toStrictEqual([new Letter('p'), new Letter('é'), new Letter('cs')]);
    });
  });
});
