import { isBindingPhonemeNeeded } from '../util/isBindingPhonemeNeeded';
import Word from '../Word';
import { Inflection } from '../../types';

describe.each([
  ['Kapos', 'Superessivus', true],
  ['Baks', 'Accusativus', true],
  ['Zalacsány', 'Accusativus', false],
  ['Barcs', 'Accusativus', true],
])('%p case', (word, inflectionType, expected) => {
  it(`${word} in ${inflectionType} should return ${expected}`, () => {
    const wordInstance = new Word(word);
    expect(isBindingPhonemeNeeded(wordInstance, inflectionType as Inflection)).toBe(expected);
  });
});
