function assertLetter(letter: string): void {
  const hunLetterRegex = /^([a-záéíóöőúüű]|[cz]s|[glnt]y|[sd]z|dzs)$/;

  if (!hunLetterRegex.test(letter)) {
    throw new TypeError(
      `Cannot use "${letter}" as Letter. Not a valid lowercase hungarian letter.`,
    );
  }
}

export default class Letter {
  value: string;

  constructor(letter: string) {
    const lowerCased = letter.toLowerCase();

    assertLetter(lowerCased);

    this.value = lowerCased;
  }

  get isVowel(): boolean {
    return /^([aáeéiíoóöőuúüű])$/.test(this.value);
  }

  get isBackVowel(): boolean {
    return /^([aáuúoó])$/.test(this.value);
  }

  get isFrontVowel(): boolean {
    return /^([eéiíüűöő])$/.test(this.value);
  }

  get isRoundedVowel(): boolean {
    return /^([öőüű])$/.test(this.value);
  }

  get isLongVowel(): boolean {
    return /^([áéíóőúű])$/.test(this.value);
  }

  get isConsonant(): boolean {
    return /^([bcdfghjklmnpqrstvxzwy]|[cz]s|[glnt]y|[sd]z|dzs)$/.test(this.value);
  }

  get opposite(): string {
    if (!this.isVowel) {
      throw new TypeError('You can call "Letter.opposite" only on vowels');
    }
    return Letter.diacriticsOpposite(this);
  }

  get longConsonant(): string {
    if (!this.isConsonant) {
      throw new TypeError('You can call "Letter.longConsonant" only on consonants');
    }

    const { value: v } = this;

    if (v.length === 1) return v + v;
    if (v.length === 2) return v[0] + v[0] + v[1];
    return v[0] + v[0] + v[1] + v[2];
  }

  is(str: string): boolean {
    return this.value === str;
  }

  static diacriticsOpposite(vowel: string | Letter): string {
    const character = vowel instanceof Letter ? vowel.value : vowel;
    const vowelPairs = 'aáeéiíoóöőuúüű'.split('');
    const index = vowelPairs.findIndex((curr) => curr === character);
    return vowelPairs[index + (index % 2 ? -1 : 1)];
  }
}
