import Word from '../Word';
import { Inflection } from '../../types';

export function isBindingPhonemeNeeded(word: Word, inflection: Inflection): boolean {
  if (word.lastLetter.isVowel) {
    return false;
  }

  if (inflection === 'Accusativus') {
    return !word.test(/(n|r|l|j|z|ny?|föld|[^kc]s)$/);
  }

  return true;
}
