import { Inflection, VowelHeight } from '../../types';

const suffixMap: { [key in Inflection]?: string } = {
  Ablativus: 't[óőő]l',
  Accusativus: '{[oeö]}t',
  Adessivus: 'n[aee]k',
  Adjectivus: 'i',
  Allativus: 'h[oeö]z',
  CasualisFinalis: 'ért',
  Dativus: 'n[aee]k',
  Delativus: 'r[óőő]l',
  Elativus: 'b[óőő]l',
  EssivusFormalis: 'ként',
  EssivusModalis: '[uüü]l',
  Illativus: 'b[aee]',
  Inessivus: 'b[aee]n',
  InstrumentalisComitativus: '(v)[aee]l',
  Locativus: '[oeö]tt',
  Sublativus: 'r[aee]',
  Superessivus: '{[oeö]}n',
  Terminativus: 'ig',
  Translativus: '(v)[áéé]',
};

function getVowelIndex(vowelHeight: VowelHeight): number {
  return { Low: 0, High: 1, HighRounded: 2 }[vowelHeight];
}

/**
 * Visszatér a megfelelő toldalákkal a suffixMap-ről
 * pl. (r)[aee]l -> ral
 */
export function getSuffix(
  inflection: Inflection,
  vowelHeight: VowelHeight,
  needConjunction = false,
  needBindingPhoneme = true,
): string {
  const unprocessedSuffix = suffixMap[inflection] as string;
  const vowelIndex = getVowelIndex(vowelHeight);

  let string: string = unprocessedSuffix;
  const inBrackets = /\[([^)]+)]/.exec(string);

  if (inBrackets) {
    const vowel = inBrackets[1].split('')[vowelIndex];
    string = string.replace(inBrackets[0], vowel);
  }

  const inParenthesis = /\(([^)]+)\)/.exec(string);

  if (inParenthesis) {
    const conjunction = inParenthesis[1];
    string = string.replace(inParenthesis[0], needConjunction ? conjunction : '');
  }

  const inBraces = /{([^)]+)}/.exec(string);

  if (inBraces) {
    const bindingPhoneme = inBraces[1];
    string = string.replace(inBraces[0], needBindingPhoneme ? bindingPhoneme : '');
  }

  return string;
}
