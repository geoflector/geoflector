import Word from '../Word';
import { VowelHarmony } from '../../types';

/** Vegyes hangrendű helységnevek, amikhez magas hangrendű toldalék társul. */
const FRONT_VOWEL_SUFFIX = ['Tiszavid', 'Somogyszil', 'Hosszúvíz'];

/** Vegyes hangrendű helységnevek, amikhez mély hangrendű toldalék társul. */
const BACK_VOWEL_SUFFIX = ['Mártély', 'Zsámbék', 'Garé', 'Kisnamény', 'Megyehíd'];

/** Visszaadja egy szó magánhangzó-harmóniáját */
export function getVowelHarmony(word: Word): VowelHarmony {
  if (FRONT_VOWEL_SUFFIX.includes(word.value)) {
    return 'Front';
  }

  if (BACK_VOWEL_SUFFIX.includes(word.value)) {
    return 'Back';
  }

  const wordVowels = word.vowels;

  if (wordVowels.length === 0) {
    return 'Front';
  }

  if (wordVowels.filter((vowel) => vowel.isBackVowel).length === 0) {
    return 'Front';
  }

  if (wordVowels.filter((vowel) => vowel.isFrontVowel).length === 0) {
    return 'Back';
  }

  return 'Mixed';
}
