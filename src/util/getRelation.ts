import Word from '../Word';
import { Relation } from '../../types';

/** Helységnevek, amikhez a belviszonyt jelölő rag társul */
const INNER_RELATION_EXCEPTIONS = [
  'Bocskaikert',
  'Bodrog',
  'Büssü',
  'Csányoszró',
  'Csököly',
  'Demecser',
  'Devecser',
  'Eger',
  'Fácánkert',
  'Folyás',
  'Garé',
  'Gelej',
  'Gesztely',
  'Győrság',
  'Győrzámoly',
  'Hernád',
  'Hidas',
  'Hidegség',
  'Igal',
  'Isztimér',
  'Kapoly',
  'Karancsság',
  'Kaszaper',
  'Kemendollár',
  'Kisbudmér',
  'Kistokaj',
  'Kiszombor',
  'Kocsér',
  'Konyár',
  'Mezőnagymihály',
  'Nagykozár',
  'Nagyszékely',
  'Nagytilaj',
  'Nógrádmarcal',
  'Noszvaj',
  'Nyírcsaholy',
  'Nyírpilis',
  'Őr',
  'Pusztazámor',
  'Rinyabesenyő',
  'Rozsály',
  'Sávoly',
  'Sikátor',
  'Simaság',
  'Sobor',
  'Somberek',
  'Somogyszil',
  'Szajol',
  'Szakály',
  'Szakmár',
  'Szakoly',
  'Szedres',
  'Szegilong',
  'Szirmabesenyő',
  'Tilaj',
  'Tiszaderzs',
  'Tisztaberek',
  'Tivadar',
  'Tokaj',
  'Tomor',
  'Várbalog',
  'Vasszentmihály',
  'Vászoly',
  'Vének',
  'Vinár',
  'Vizsoly',
  'Völcsej',
  'Zalakomár',
  'Zalaszentmihály',
  'Ziliz',
  'Zselicszentpál',
  'Zubogy',
];

/** Helységnevek, amikhez külviszonyt jelölő rag társul */
const INNER_RELATION_INCLUSION = [
  'Balaton',
  'Bazsi',
  'Bugyi',
  'Dunatetétlen',
  'Ecseny',
  'Gagyapáti',
  'Hajdúhadház',
  'Hajdúsámson',
  'Halászi',
  'Iván',
  'Jászivány',
  'Kám',
  'Kilimán',
  'Kisunyom',
  'Köröm',
  'Mezőgyán',
  'Nagykereki',
  'Nyim',
  'Osli',
  'Pári',
  'Pilisszentiván',
  'Pusztavám',
  'Rábcakapi',
  'Rém',
  'Rum',
  'Sarkadkeresztúr',
  'Sárkeresztúr',
  'Som',
  'Somogyhatvan',
  'Somogysámson',
  'Szentistván',
  'Szentkatalin',
  'Szin',
  'Terem',
  'Tetétlen',
  'Tiszapüspöki',
  'Torony',
  'Újszentiván',
  'Velem',
];

export function getRelation(word: Word): Relation {
  const { value } = word;
  const INNER_RELATION_ENDS_WITH_RE = /(város|keresztúr|[Gg]yőr|abar|tér|dor|bátor|kúti|major|ény|ny|[mni])$/;
  const INNER_RELATION_NOT_ENDS_WITH_RE = /(vár|lak|szombat|hely|falu|keszi|ly|[aáeuj])$/;

  if (INNER_RELATION_EXCEPTIONS.includes(value)) {
    return 'Inner';
  }

  if (INNER_RELATION_INCLUSION.includes(value)) {
    return 'Outer';
  }

  if (word.test(INNER_RELATION_ENDS_WITH_RE) && !word.test(INNER_RELATION_NOT_ENDS_WITH_RE)) {
    return 'Inner';
  }

  return 'Outer';
}
