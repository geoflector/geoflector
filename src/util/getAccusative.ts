import Word from '../Word';

export function getAccusative(word: Word): Word {
  if (word.test(/ház$/)) return word.replace(/ház$/, 'háza');
  if (word.test(/halom$/)) return word.replace(/halom$/, 'halm');
  if (word.test(/föld$/)) return word.replace(/föld$/, 'földe');

  return word;
}
