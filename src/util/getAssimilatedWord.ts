import Word from '../Word';
import { Inflection } from '../../types';
import { shouldAssimilate } from './shouldAssimilate';
import { getAccusative } from './getAccusative';

function shouldReplaceEndingVowelToOpposite(word: Word) {
  const { lastLetter } = word;
  return !lastLetter.isLongVowel && !word.test(/([iüu]|ház|föld)$/);
}

export function getAssimilatedWord(word: Word, inflection: Inflection): Word {
  let wordStem: Word = word;

  if (inflection === 'Accusativus') {
    wordStem = getAccusative(word);
  }

  if (!shouldAssimilate(wordStem, inflection)) {
    return wordStem;
  }

  const letters = [...wordStem.letters].map((letter) => letter.value);
  const { lastLetter, lastVowel } = wordStem;

  if (lastLetter.isConsonant) {
    letters.pop();
    letters.push(lastLetter.longConsonant);
  } else if (shouldReplaceEndingVowelToOpposite(word)) {
    letters.pop();
    letters.push(lastVowel.opposite);
  }

  return new Word(letters.join(''));
}
