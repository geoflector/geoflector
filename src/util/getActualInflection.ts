import { Inflection } from '../../types';
import Word from '../Word';
import { shouldAnalyzeRelation } from './shouldAnalyzeRelation';
import { getRelation } from './getRelation';
import { hasLocativeCase } from './hasLocativeCase';

export function getActualInflection(word: Word, inflection: Inflection): Inflection {
  if (!shouldAnalyzeRelation(inflection)) return inflection;
  const relation = getRelation(word);

  switch (inflection) {
    case 'ElativusOrDelativus':
      return relation === 'Inner' ? 'Elativus' : 'Delativus';
    case 'IllativusOrSublativus':
      return relation === 'Inner' ? 'Illativus' : 'Sublativus';
    case 'InessivusOrSuperessivus':
      return relation === 'Inner' ? 'Inessivus' : 'Superessivus';
    case 'Locativus':
      if (hasLocativeCase(word)) return inflection;
      return relation === 'Inner' ? 'Inessivus' : 'Superessivus';
    default:
      return inflection;
  }
}
