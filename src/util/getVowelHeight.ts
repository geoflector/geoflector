import Word from '../Word';
import { getVowelHarmony } from './getVowelHarmony';
import { VowelHeight } from '../../types';

export function getVowelHeight(word: Word): VowelHeight {
  if (word.lastVowel.isRoundedVowel) {
    return 'HighRounded';
  }

  const vowelHarmony = getVowelHarmony(word);

  if (vowelHarmony === 'Back') {
    return 'Low';
  }

  if (vowelHarmony === 'Front') {
    return 'High';
  }

  if (vowelHarmony === 'Mixed') {
    const lastTwoVowels = word.vowels.slice(-2);

    if (/[eé]/.test(word.lastVowel.value) || lastTwoVowels.every((vowel) => vowel.isFrontVowel)) {
      return 'High';
    }

    return 'Low';
  }

  return 'Low';
}
