import Word from '../Word';

/**
 * Olyan földrajzi nevek, ahol még aktívan használja a magyar
 * nyelv a locativus esetet
 *
 * @see {@link https://hu.wikipedia.org/wiki/Locativus|Wikipedia}
 */
export function hasLocativeCase(word: Word): boolean {
  return ['Győr', 'Kaposvár', 'Kolozsvár', 'Marosvásárhely', 'Pécs', 'Székesfehér', 'Vác'].includes(
    word.value,
  );
}
