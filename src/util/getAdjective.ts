import Word from '../Word';

/**
 * Ezek a helységnevek az általános szabály ellenére
 * "-házi" végződést kapnak, a "-házai" helyett
 */
const ADJECTIVE_EXCEPTIONS = ['Ágasegyháza', 'Himesháza', 'Kiskunfélegyháza'];

export function getAdjective(word: Word): string {
  if (word.value === 'Eger') return 'egri';
  if (word.lastLetter.value === 'i') return word.lowerCase;
  if (/halom$/.test(word.lowerCase)) return word.lowerCase.replace(/halom$/, 'halmi');
  if (ADJECTIVE_EXCEPTIONS.includes(word.value)) return word.lowerCase.replace(/háza$/, 'házi');
  return word.lowerCase + 'i';
}
