import { Inflection } from '../../types';

export function shouldAnalyzeRelation(inflection: Inflection): boolean {
  return [
    'ElativusOrDelativus',
    'IllativusOrSublativus',
    'InessivusOrSuperessivus',
    'Locativus',
  ].includes(inflection);
}
