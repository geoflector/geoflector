import Word from '../Word';
import { Inflection } from '../../types';

function shouldAssimilateWithConsonantEnding(inflection: Inflection): boolean {
  return ['InstrumentalisComitativus', 'Translativus'].includes(inflection);
}

function shouldAssimilateWithVowelEnding(inflection: Inflection): boolean {
  return !['Nominativus', 'EssivusFormalis', 'Adjectivus'].includes(inflection);
}

export function shouldAssimilate(word: Word, inflection: Inflection): boolean {
  return word.lastLetter.isConsonant
    ? shouldAssimilateWithConsonantEnding(inflection)
    : shouldAssimilateWithVowelEnding(inflection);
}
