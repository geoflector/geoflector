export type VowelHeight = 'High' | 'Low' | 'HighRounded';
export type VowelHarmony = 'Back' | 'Front' | 'Mixed';
export type Relation = 'Inner' | 'Outer';
export type Inflection =
  | 'Ablativus'
  | 'Accusativus'
  | 'Adessivus'
  | 'Adjectivus'
  | 'Allativus'
  | 'CasualisFinalis'
  | 'Dativus'
  | 'Delativus'
  | 'Elativus'
  | 'ElativusOrDelativus'
  | 'EssivusFormalis'
  | 'EssivusModalis'
  | 'Illativus'
  | 'IllativusOrSublativus'
  | 'Inessivus'
  | 'InessivusOrSuperessivus'
  | 'InstrumentalisComitativus'
  | 'Locativus'
  | 'Nominativus'
  | 'Sublativus'
  | 'Superessivus'
  | 'Terminativus'
  | 'Translativus';

declare class GeoFlector {
  constructor(geographicalName: string);
  getInflection(inflection: Inflection): string;
}

export default GeoFlector;
